package com.kytms.rule.core;

import com.kytms.rule.core.exception.RuleException;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * 缓存规则
 *
 * @author 臧英明
 * @create 2018-05-07
 */
public interface CacheRule<G> extends Rule {
    /**
     * 根据Id执行缓存中的规则
     * @param id
     * @return
     */
    Object executeRule(String id) throws RuleException;

    /**
     * 判定缓存中的规则是否存在
     * @return
     */
    boolean isExist(String id);

    /**
     * 保存一个规则
     * @param id
     * @param ruleModel
     */
    void saveRule(String id,RuleModel ruleModel);

    /**
     * 获取一个规则
     * @return
     */
    RuleModel getRule(String id);
}
